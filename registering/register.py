'''
Created on Aug 14, 2014

@author: phe
'''
from multiprocessing import process
import os
import glob
import cv2
import cPickle
from proc.zern import ZernikeMoments
from proc.masterProc import ProcMaster

if not __name__ == '__main__':
    
    class RegisDataset(object):
        '''
        classdocs
        '''
        def __init__(self, dataset, index, blobpath):
            '''
            Constructor
            '''
            self.dataset = dataset
            self.index = index
            if not self.index.endswith('.phedex'):
                self.index += ".phedex"
            self.blobpath = self.defaultBlob() if blobpath is None else self.verfifyBlobPath(blobpath)
            self.checkBlobPath(blobpath)
        
        def defaultBlob(self):
            return "blob/"
        
        def checkBlobPath(self, blobpath):
            if not "/" in blobpath[-1]:
                self.blobpath += "/"
        
        def verfifyBlobPath(self, blobpath):
            if os.path.exists(blobpath):
                return blobpath
            else:
                os.makedirs(blobpath)
                return blobpath
        
        def regis(self):
            try:
                desc = ZernikeMoments(150)
                binary = {}
                alpha = 1
                beta = (1-alpha)
                for imgList in glob.glob(self.dataset + "/*.png"):
                    indexname =imgList[imgList.rfind("/") +1:]
                    filename = indexname.replace(".png", "")
                    image = cv2.imread(imgList)
                    image = ProcMaster().resize(image, 50, 50)
                    weighted = cv2.addWeighted(image, alpha, image, beta, 0)
                    # weighted = cv2.bilateralFilter(weighted, 255, 75, 75)
                    image = ProcMaster().proc_to_comp(weighted)
                    # image = ProcMaster().resize(image, 300, 300)
                    cv2.imwrite(self.blobpath+filename+ ".png", image)
                    moments = desc.describe(image)
                    binary[indexname] = moments
                f = open(self.index, "w")
                f.write(cPickle.dumps(binary))
                f.close()
            except Exception, err:
                print "uh oh, the lazy programmer made this error. hurry up call the police "+str(err)
                return -2
            return 0