'''
Created on Aug 13, 2014

@author: phe
'''
from register import RegisDataset as rs

if __name__ == '__main__':
    print "hello world"
else:
    class RegisDataset(object):
        '''
        classdocs
        '''
        def __init__(self, dataset, index, blobpath):
            '''
            Constructor
            '''
            self.dataset = dataset
            self.index = index
            self.blobpath = blobpath
        
        def regis(self):
            return rs(self.dataset, self.index, self.blobpath).regis()