'''
Created on Aug 14, 2014

@author: phe
'''

import cv2
import glob
from scipy.spatial import distance as dist

if not __name__ == '__main__':
    class ColorMatch(object):
        '''
        classdocs
        '''


        def __init__(self, params):
            '''
            Constructor
            '''

        @staticmethod
        def color_match(image_path, imagenya, method):
            index = {}
            SCIPY_METHODS = (
                             ("Euclidean", dist.euclidean),
                             ("Manhattan", dist.cityblock),
                             ("Chebysev", dist.chebyshev))
            OPENCV_METHODS = (
                              ("Correlation", cv2.cv.CV_COMP_CORREL),
                              ("Chi-Squared", cv2.cv.CV_COMP_CHISQR),
                              ("Intersection", cv2.cv.CV_COMP_INTERSECT),
                              ("Hellinger", cv2.cv.CV_COMP_BHATTACHARYYA))
            for path in glob.glob(image_path + "/*.png"):
                image = cv2.imread(path)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
                hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
                hist = cv2.normalize(hist).flatten()
                index[path[path.rfind("/") + 1:]] = hist
            proc = imagenya.copy()
            proc = cv2.resize(proc, (300,300))
            proc = cv2.resize(proc, (50,50), interpolation=cv2.INTER_AREA)
            proc = cv2.GaussianBlur(proc, (0, 0), 3)
            proc = cv2.addWeighted(proc, 1, proc, 0.5, 0)
            proc = cv2.resize(proc, (300, 300), interpolation=cv2.INTER_AREA)
            proc = cv2.cvtColor(proc, cv2.COLOR_BGR2LAB)
            hist_imagenya = cv2.calcHist([proc], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
            hist_imagenya = cv2.normalize(hist_imagenya).flatten()
            returnnya = {}
            if(method.lower() == "scipy" or method.lower() == "s"):
                for (methodname, methodnya) in SCIPY_METHODS:
                    results = {}
                    for (k, hist) in index.items():
                        d = methodnya(hist_imagenya, hist)
                        results[k] = d                    
                    results = sorted([(v, k) for (k, v) in results.items()])
                    returnnya[methodname] = results
                return returnnya
            elif(method.lower() == "opencv" or method.lower() == "o"):
                for(methodname, methodnya) in OPENCV_METHODS:
                    results = {}
                    reverse = False
                    if methodname in ("Correlation", "Intersection"):
                        reverse = True
                    for(k, hist) in index.items():
                        d = cv2.compareHist(hist_imagenya, hist, methodnya)
                        results[k] = d
                    results = sorted([(v, k) for (k, v) in results.items()], reverse=reverse)
                    returnnya[methodname] = results

                return returnnya
            else:
                return False