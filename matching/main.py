'''
Created on Aug 12, 2014

@author: phe
'''
import os
from matching.colormatch import ColorMatch as cm
from proc.masterProc import ProcMaster as pm
import datetime
import random

if __name__ == '__main__':
    print "hello world"
else:
    class Phemplatematch:
        '''
        classdocs
        '''

        def __init__(self, image_path, imagenya, method, phedex):
            '''
            Constructor
            '''
            self.image_path = image_path
            self.imagenya = imagenya
            self.method = method
            self.phedex = phedex
            self.verified = self.verified()

        def verified(self):
            if self.verify_path() and self.verify_file() and self.verify_phedex():
                return True
            else:
                return -1
            
        def verify_path(self):
            if os.path.exists(self.image_path):
                self.check_path_end()
                return True
            else:
                return False
            
        def verify_file(self):
            if os.path.isfile(self.imagenya) and os.access(self.imagenya, os.R_OK) and self.check_file_type():
                return True
            else:
                return False

        def verify_phedex(self):
            self.check_phedex()
            if os.path.isfile(self.phedex) and os.access(self.phedex, os.R_OK):
                return True
            else:
                return False

        def check_phedex(self):
            if not self.phedex.endswith('.phedex'):
                self.phedex += ".phedex"

        def check_path_end(self):
            if not "/" in self.image_path[-1]:
                self.image_path += "/"
        
        def check_file_type(self):
            if not self.imagenya.endswith('.png'):
                return False
            else:
                return True
                
        def color_match(self):
            self.bin_listnya = cm.color_match(self.image_path, self.cropped_image, self.method)
            if not self.bin_listnya:
                return False
            else:
                return True

        def crop_image(self):
            return pm.cropping3(self.imagenya)

        def get_list_bin(self):
            if not self.color_match():
                return False
            else:
                self.sort_list_bin()
                return True

        def sort_list_bin(self):
            self.listnya = pm.histo_check2(self.bin_listnya)

        def list_bin(self):
            if self.get_list_bin():
                pass
            else:
                return True

        def make_barbuk(self, foldernya):
            head, tail = os.path.splitext(os.path.basename(self.imagenya))
            if os.path.exists(foldernya):
                pass
            else:
                os.makedirs(foldernya)
            pm.save_barbuk(foldernya + head + "_" + str(random.randrange(0,99)) + tail, self.cropped_image)
            pass

        def compare(self):
            today = datetime.date.today()
            if not self.verified == -1:
                self.cropped_image = self.crop_image()
                if not isinstance(self.cropped_image, (int, long)):
                    foldernya = "barbuk/" + today.strftime('%d') + today.strftime('%b') + "/"
                    self.make_barbuk(foldernya)
                    pass
                else:
                    return self.cropped_image
                if not self.list_bin():
                    pass
                else:
                    return -2
                return pm.do_compare(self.cropped_image, self.phedex, self.listnya)
            else:
                return self.verified
