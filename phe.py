'''
Created on Aug 12, 2014

@author: phe
'''

#!/usr/bin/env python
import time
import os
import argparse
from matching.main import Phemplatematch
from registering.main import RegisDataset

start_time = time.time()

def register(dataset, index, blobpath="/"):
    return RegisDataset(dataset, index, blobpath).regis()

if __name__ == '__main__':
    # def histo_check(dataset, imagenya, metodenya):
    #     color_hist = Phemplatematch(dataset, imagenya, metodenya).color_match()
    #     if(color_hist == -1):
    #         print "MNNS" #method name not specified
    #         exit()
    #     elif(color_hist == -2):
    #         print "FE" #path error
    #         exit()
    #     unique_a = []
    #     for idx, method_name in enumerate(color_hist):
    #         temp_v = color_hist[method_name][slice(0,5)]
    #         if(idx == 0):
    #             temp = temp_v
    #         else:
    #             unique_s = list(set([v for (k,v) in temp] + [v for (k,v) in temp_v]))
    #             unique_a = list(set(unique_a + unique_s))
    #             temp = temp_v
    #
    #     print json.dumps(unique_a, separators=('|',':'))

    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--dataset", required = True,
                    help = "chose the data image library")
    ap.add_argument("-f", "--file", required = True,
                    help = "Path where the file is")
    ap.add_argument("-c", "--comparemethod", required = True,
                    help = "Compare Hist method")
    ap.add_argument("-i", "--index", required = True,
                    help = "index file to compare (*.phedex)")
    args = vars(ap.parse_args())

    returnnya = Phemplatematch(args['dataset'], args['file'], args['comparemethod'], args['index']).compare()

    print returnnya
    print("--- done in {0} seconds ---".format((time.time() - start_time)))
else:
    pass