'''
Created on Aug 13, 2014

@author: phe
'''
import os
import cv2
import numpy as np
from zern import Searcher, ZernikeMoments
import cPickle
from color_transfer import color_transfer

class ProcMaster(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    @staticmethod
    def proc_to_comp2(image):
        source = cv2.imread('kotak.png')
        image_tmp = image.copy()
        transfer = color_transfer(source, image_tmp)
        transfer = cv2.bitwise_not(transfer)
        blur = cv2.cvtColor(transfer, cv2.COLOR_BGR2GRAY)
        transfer = blur
        transfer = cv2.GaussianBlur(transfer, (3, 3), 0)
        transfer = cv2.Canny(transfer, 70, 90)
        transfer = cv2.dilate(transfer, None)

        (cnts, _) = cv2.findContours(transfer.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[0]
        outline = np.zeros(image.shape, dtype="uint8")
        cv2.drawContours(outline, cnts, -1, 255, -1)
        outline = cv2.cvtColor(outline, cv2.COLOR_BGR2GRAY)
        outline = cv2.adaptiveThreshold(outline, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 77, 0)

        return outline

    @staticmethod
    def proc_to_comp(image):
        source = cv2.imread('kotak.png')
        image_tmp = image.copy()
        transfer = color_transfer(source, image_tmp)
        transfer = cv2.GaussianBlur(transfer, (9, 9), 0)
        transfer = cv2.bitwise_not(transfer)
        blur = cv2.cvtColor(transfer, cv2.COLOR_BGR2GRAY)
        blur = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 77, 0)
#     transfer = cv2.cvtColor(transfer, cv2.COLOR_BGR2GRAY)
#     transfer = cv2.bitwise_not(transfer)
#     # transfer = cv2.GaussianBlur(transfer, (37, 37), -1)
#     kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT,(1024,1024))
#     close = cv2.morphologyEx(transfer,cv2.MORPH_CLOSE,kernel1)
#     div = np.float32(transfer)/(close)
#     res = np.uint8(cv2.normalize(div,div,0,5,cv2.NORM_MINMAX))
#     res = cv2.GaussianBlur(res, (17,17), 0)
#     blur = cv2.adaptiveThreshold(res, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 1035, 0)

        return blur

    @staticmethod
    def proc_to_comp3(image):
        source = cv2.imread('kotak.png')
        image_tmp = image.copy()
        transfer = color_transfer(source, image_tmp)
        transfer = cv2.GaussianBlur(transfer, (9, 9), 0)
        transfer = cv2.bitwise_not(transfer)
        blur = cv2.cvtColor(transfer, cv2.COLOR_BGR2GRAY)
        blur = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 77, 0)

        test = cv2.Canny(blur, 0, 0)
        cv2.imshow('test', test)
        itunya = np.zeros(image.shape, dtype="uint8")
        (cnts, _) = cv2.findContours(test.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        for cnt in cnts:
            cv2.drawContours(itunya, [cnt], 0, (255,0,0), 1)
        itunya = cv2.adaptiveThreshold(cv2.cvtColor(itunya, cv2.COLOR_RGB2GRAY), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 77, 0)

        return itunya

    @staticmethod
    def cropping(imagePath):
        image = cv2.imread(imagePath)
        go = image.copy()
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        thresh = cv2.bitwise_not(image)
        thresh = cv2.GaussianBlur(thresh, (9,9), 0)
        thresh = cv2.Canny(thresh, 30,70)
        thresh = cv2.dilate(thresh, None)
        thresh = cv2.adaptiveThreshold(thresh, 126, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)
        (cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        draw = np.zeros(shape=(0,0,0), dtype="uint8")
        for cnt in cnts:
            hull = cv2.convexHull(cnt)
            hull = cv2.approxPolyDP(hull, 0.1 * cv2.arcLength(hull, True), True)
            if len(hull) == 4:
                draw = hull
        (height, width, depth) = draw.shape
        if not height == 4:
            return -3
        pts = draw.reshape(4,2)
        rect = np.zeros((4,2), dtype="float32")
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        (TL, TR, BR, BL) = rect
        src = np.atleast_3d(([TL], [TR], [BR], [BL])).astype('float32')
        w, l = (50, 50)
        dst = np.array([
                        [-10, -10],
                        [l+10, -10],
                        [l+10, w+10],
                        [-10, w+10]], dtype="float32")
        perspec = cv2.getPerspectiveTransform(src, dst)
        square = np.zeros(shape=(50,50), dtype="uint8")
        warp_it = cv2.warpPerspective(go, perspec, (l, w), square, cv2.INTER_NEAREST)
        cv2.imwrite('ini.png', warp_it)
        return warp_it

    @staticmethod
    def cropping2(imagePath):
        image = cv2.imread(imagePath)
        go = image.copy()
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        thresh = cv2.bitwise_not(image)
        thresh = cv2.GaussianBlur(thresh, (9,9), 0)
        thresh = cv2.Canny(thresh, 30,70)
        thresh = cv2.dilate(thresh, None)
        thresh = cv2.adaptiveThreshold(thresh, 126, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)
        (cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        draw = np.zeros(shape=(0,0,0), dtype="uint8")
        for cnt in cnts:
            hull = cv2.convexHull(cnt)
            hull = cv2.approxPolyDP(hull, 0.1 * cv2.arcLength(hull, True), True)
            if len(hull) == 4:
                draw = hull
        (height, width, depth) = draw.shape
        if not height == 4:
            return -3
        pts = draw.reshape(4,2)
        rect = np.zeros((4,2), dtype="float32")
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        (TL, TR, BR, BL) = rect
        src = np.atleast_3d(([TL], [TR], [BR], [BL])).astype('float32')
        w, l = (50, 50)
        dst = np.array([
                        [0, 0],
                        [l, 0],
                        [l, w],
                        [0, w]], dtype="float32")
        perspec = cv2.getPerspectiveTransform(src, dst)
        square = np.zeros(shape=(50,50), dtype="uint8")
        warp_it = cv2.warpPerspective(go, perspec, (l, w), square, cv2.INTER_NEAREST)
        outline = cv2.resize(warp_it, (300, 300))
        cv2.imwrite('ini.png', outline)
        return warp_it

    @staticmethod
    def cropping3(imagePath):
        image = cv2.imread(imagePath)
        source = cv2.imread('kotak.png')
        go = image.copy()

        transfer = color_transfer(source, image)
        transfer = cv2.cvtColor(transfer, cv2.COLOR_BGR2GRAY)
        transfer = cv2.bitwise_not(transfer)
        transfer = cv2.GaussianBlur(transfer, (37, 37), -1)
        kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT,(1024,1024))
        close = cv2.morphologyEx(transfer,cv2.MORPH_CLOSE,kernel1)
        div = np.float32(transfer)/(close)
        res = np.uint8(cv2.normalize(div,div,0,5,cv2.NORM_MINMAX))
        # res = cv2.GaussianBlur(res, (17,17), 0)
        res = cv2.adaptiveThreshold(res, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 1035, 0)

        (cnts, _) = cv2.findContours(res.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        outline = np.zeros(image.shape, dtype="uint8")
        nex = 0
        for cnt in cnts:
            current = cv2.contourArea(cnt)
            hull = cv2.convexHull(cnt)
            hull = cv2.approxPolyDP(hull, 0.1 * cv2.arcLength(hull, True), True)
            if(current > nex):
                if len(hull) == 4:
                    draw = hull
                    nex = current

        cv2.drawContours(outline, hull, -1, 255, 1)
        pts = draw.reshape(4, 2)
        rect = np.zeros((4, 2), dtype="float32")
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        (TL, TR, BR, BL) = rect

# src = np.atleast_3d(([TL], [TR], [BR], [BL])).astype('float32')
# w, l = (50, 50)
# dst = np.array([
# [-5, -5],
# [l+5, -5],
# [l+5, w+5],
# [-5, w+5]], dtype="float32")
#
# test = cv2.getPerspectiveTransform(src, dst)
# out = np.zeros(shape=(50, 50), dtype="uint8")
# warp = cv2.warpPerspective(go, test, (l, w), out, cv2.INTER_NEAREST)

        widthA = np.sqrt(((BR[0] - BL[0]) ** 2) + ((BR[0] - BL[0]) ** 2))
        widthB = np.sqrt(((TR[0] - TL[0]) ** 2) + ((TR[0] - TL[0]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        heightA = np.sqrt(((TR[1] - BR[1]) ** 2) + ((TR[1] - BR[1]) ** 2))
        heightB = np.sqrt(((TL[1] - BL[1]) ** 2) + ((TL[1] - BL[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        src = np.atleast_3d(([TL], [TR], [BR], [BL])).astype('float32')
        w, h = (maxWidth, maxHeight)
        dst = np.array([
        [-15, -15],
        [w+14, -15],
        [w+14, h+14],
        [-15, h+14]], dtype="float32")

        test = cv2.getPerspectiveTransform(src, dst)
        warp = cv2.warpPerspective(go, test, (maxWidth, maxHeight))
        warp = cv2.resize(warp, (50,50))

        return warp


    @staticmethod
    def histo_check(dataset):
        unique_a = []
        for idx, method_name in enumerate(dataset):
            temp_v = dataset[method_name][slice(0,5)]
            if(idx == 0):
                temp = temp_v
            else:
                unique_s = list(set([v for (k,v) in temp] + [v for (k,v) in temp_v]))
                unique_a = list(set(unique_a + unique_s))
                temp = temp_v

        return unique_a

    @staticmethod
    def histo_check2(dataset, methodnya="Hellinger"):
            return list(v for (k,v) in dataset[methodnya][slice(0,5)])

    @staticmethod
    def do_compare(imagenya, indexnya, listnya):
        alpha = 1
        beta = (1 - alpha)
        index = open(indexnya).read()
        index = cPickle.loads(index)
        index = {k: index[k] for k in listnya if k in index}
        weighted = cv2.addWeighted(imagenya, alpha, imagenya, beta, 0)
        # weighted = cv2.bilateralFilter(weighted, 255, 75, 75)
        outline = ProcMaster.proc_to_comp(weighted)
        # outline = cv2.resize(outline, (300, 300))

        desc = ZernikeMoments(150)
        query = desc.describe(outline)

        search = Searcher(index)
        result = search.search(query)
        return result[0][1]

    @staticmethod
    def resize(image, width = None, height = None, inter = cv2.INTER_AREA):
        dim = None
        (h, w) = image.shape[:2]

        if width is None and height is None:
            return image

        if width is None:
            r = height / float(h)
            dim = (int(w * r), height)

        else:
            r = width / float(w)
            dim = (width, int(h * r))

        resized = cv2.resize(image, dim, interpolation = inter)

        return resized

    @staticmethod
    def save_barbuk(pathnya, barbuknya):

        cv2.imwrite(pathnya, barbuknya)