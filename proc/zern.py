'''
Created on Aug 13, 2014

@author: phe
'''

from scipy.spatial import distance as dist

import mahotas

class ZernikeMoments:
    '''
    classdocs
    '''
    def __init__(self, radius):
        self.radius = radius

    def describe(self, image):
        return mahotas.features.zernike_moments(image, self.radius)
    

class Searcher:
    def __init__(self, index):
        self.index = index

    def search(self, queryFeatures):
        results = {}
        for (k, features) in self.index.items():
            d = dist.euclidean(queryFeatures, features)
            results[k] = d
        results = sorted([(v, k) for (k, v) in results.items()])
        return results