'''
Created on Aug 13, 2014

@author: phe
'''
#!/usr/bin/env python

import argparse
import phe
import time

start_time = time.time()
if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--dataset", required = True,
                    help = "Path where the data image is")
    ap.add_argument("-i", "--index", required = True,
                    help = "Path to where the index file will be stored")
    ap.add_argument("-b", "--blob", required = False,
                    help = "Path to where the blob will be stored, need the folder there first or it will create one")
    args = vars(ap.parse_args())
    status = phe.register(args['dataset'], args['index'], args['blob'])

    if(status >= 0):
        print "ok"
        print("--- done in {0} seconds ---".format((time.time() - start_time)))
    else:
        print "ko"